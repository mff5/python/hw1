from sys import argv

number = int(argv[1])
result_justification = len(str(number * 10))
number_justification = len(str(number))

for i in range(1, 11):
    index = str(i).rjust(2)
    number_str = str(number).rjust(number_justification)
    result = str(i * number).rjust(result_justification)
    print(f"{index} * {number_str} = {result}")
